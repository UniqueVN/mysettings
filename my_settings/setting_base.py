import configparser
from typing import List
from distutils.util import strtobool
import ast

class SettingBase(object):
    """Base class for settings data object"""

    def __init__(self):
        # Set default values here
        # pass
        self._config_parser = configparser.ConfigParser()

    def __str__(self):
        str = ""
        for attr, value in self.__dict__.items():
            if (not attr.startswith('_')):
                str += "{}: {}\n".format(attr, value)
        return str

    # Update from a dictionary object
    def update_from_dict(self, dict_obj):
        if (dict_obj is None):
            return

        for attr, value in self.__dict__.items():
            # print("attr: {} - value: {}".format(attr, value))
            if (attr in dict_obj):
                new_value = dict_obj[attr]
                # print("attr: {} - value: {} - new_value:{}".format(
                #     attr, value, new_value))
                if (not new_value is None):
                    value_type = type(value)
                    # print("value_type: {}".format(value_type))
                    if (value_type is bool):
                        self.__dict__[attr] = (strtobool(new_value) == 1)
                        # print("{} is bool type - value: {}".format(attr, new_value))
                    elif (value_type is list):
                        list_value = ast.literal_eval(new_value)
                        # print("List value: {} - list_value: {}".format(new_value, list_value))
                        self.__dict__[attr] = list_value
                    else:    
                        # TODO: For the basic type convert directly like this work
                        # but it may not work for more complex type
                        self.__dict__[attr] = value_type(new_value)
        
    def update_from_object(self, other_obj):
        if (other_obj is None):
            return

        if (type(other_obj) is dict):
            self.update_from_dict(other_obj)    
        else:
            self.update_from_dict(other_obj.__dict__)
    
    def update_from_config_file_path(self, config_file_path, section_name = None):
        # TODO: Need to check the file type and use the right parser for it
        # TODO: May want to expose some adapter interface to read the config file
        # parser = configparser.ConfigParser()
        parser = self._config_parser
        parser.read(config_file_path)
        # print("parser: {} - config_file_path: {} - section_name: {}".format(
        #     parser, config_file_path, section_name))

        # Use the full config file if the section's name is not specified
        if not section_name:
            self.update_from_dict(parser)
        elif (section_name in parser):
            config_section = parser[section_name]
            self.update_from_dict(config_section)

        # print("parser: {}".format(parser[section_name].__dict__))

    def update_from_config_file(self, config_file, section_name = None):
        # TODO: Need to check the file type and use the right parser for it
        # TODO: May want to expose some adapter interface to read the config file
        # parser = configparser.ConfigParser()
        parser = self._config_parser
        parser.read_file(config_file)
        # print("parser: {} - config_file_path: {} - section_name: {}".format(
        #     parser, config_file_path, section_name))

        # Use the full config file if the section's name is not specified
        if not section_name:
            self.update_from_dict(parser)
        elif (section_name in parser):
            config_section = parser[section_name]
            self.update_from_dict(config_section)

        # print("parser: {}".format(parser[section_name].__dict__))

    def save_to_file(self, out_file_path):
        with open(out_file_path, 'w') as cfg_file:
            self._config_parser.write(cfg_file)
            # cfg_file.close()

    @staticmethod
    def create_from_config_file_path(setting_class, config_file_path, section_name = ""):
        new_config = setting_class()
        new_config.update_from_config_file_path(config_file_path, section_name)

        return new_config
    
    @staticmethod
    def create_from_config_file(setting_class, config_file, section_name = ""):
        new_config = setting_class()
        new_config.update_from_config_file(config_file, section_name)

        return new_config