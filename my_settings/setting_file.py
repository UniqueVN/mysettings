import configparser
from typing import List
from distutils.util import strtobool
import ast
from .setting_base import *
from os import path
import time
import threading

class SettingFile(SettingBase):
    """Class present setting file"""

    def __init__(self, in_file_path: str, in_section_name: str="") -> None:
        SettingBase.__init__(self)
        self._src_file_path: str = in_file_path
        self._src_section_name: str = in_section_name
        self._modified_time: float = 0
        self.update_from_config_file_path(self._src_file_path, self._src_section_name)
        update_thread = threading.Thread(target=self.check_update)
        update_thread.daemon = True
        update_thread.start()

    def update_from_config_file_path(self, config_file_path: str, section_name: str = ""):
        self._modified_time = path.getmtime(config_file_path)
        SettingBase.update_from_config_file_path(self, config_file_path, section_name)

    def is_modified(self):
        last_modified_time = path.getmtime(self._src_file_path)
        # print("self.modified_time: {} - last_modified_time: {}".format(
            # self.modified_time, last_modified_time))
        return (last_modified_time > self._modified_time)

    def check_update(self):
        while True:
            if (self.is_modified()):
                print("Config file is modified => updating ...")
                self.update_from_config_file_path(self._src_file_path, self._src_section_name)
                print("Updated:\n{}".format(self))

            time.sleep(1.0)
