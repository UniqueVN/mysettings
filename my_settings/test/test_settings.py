import time
import threading
import argparse
import json
import os
import time
from typing import List

from my_settings import *
# from setting_base import *

# ===================== Setting data  =====================
class TestSettings(SettingBase):
    def __init__(self):
        SettingBase.__init__(self)
        # Set default values here
        self.arg1: int = 1
        self.arg2: float = 2.5
        self.arg3: bool = False
        self.arg4: List[str] = ["One", "Two"]
        self.arg5: str = "test"

class TestSettingFile(SettingFile):
    def __init__(self, file_path, section_name):
        SettingFile.__init__(self, file_path, section_name)
        # Set default values here
        self.arg1: int = 1
        self.arg2: float = 2.5
        self.arg3: bool = False
        self.arg4: List[str] = ["One", "Two"]
        self.arg5: str = "test"
    
# ===================== Test =====================
def test():
    DEFAULT_CONFIG_FILE = "./config/config.ini"
    DEFAULT_CONFIG_FILE_OUT = "./config/config_out.ini"
    DEFAULT_JSON_CONFIG_FILE = "config/config.json"

    settings = TestSettings()
    print("Default settings: {}".format(settings))

    # settings.update_from_config_file(DEFAULT_CONFIG_FILE, "Test")
    script_dir = os.path.dirname(__file__)
    test_ini_path = os.path.join(script_dir, DEFAULT_CONFIG_FILE)
    settings = TestSettings.create_from_config_file_path(TestSettings, test_ini_path, "Test")
    settings.arg1 -= 20
    print("settings:\n{}".format(settings))

    test_json_config_path = os.path.join(script_dir, DEFAULT_JSON_CONFIG_FILE)
    print("test_json_config_path: {}".format(test_json_config_path))
    with open(test_json_config_path) as json_file:
        json_data = json.load(json_file)

    settings.update_from_object(json_data)
    print("settings:\n{}".format(settings))

    arg_parser = argparse.ArgumentParser(description="Test settings data")
    arg_parser.add_argument('-c', '--config_file', type=argparse.FileType('r'), default=None)
    arg_parser.add_argument('-v', '--version', action='version', version='0.0.1')
    arg_parser.add_argument('--arg1', type=int, help="First argument")
    arg_parser.add_argument('--arg2', type=int, help="Second argument", default=15)
    args = arg_parser.parse_args()

    config_file = args.config_file
    if not (config_file is None):
        settings.update_from_config_file(config_file, "Test")
        print("Update settings from new config file: {}\n {}".format(config_file.name, settings))
    
    print("args = {}\n".format(args))
    print("args2 + 10 = {}".format(args.arg2 + 10))

    settings.update_from_object(args)
    print("settings:\n{}".format(settings))

    test_ini_out_path = os.path.join(script_dir, DEFAULT_CONFIG_FILE_OUT)
    settings.save_to_file(test_ini_out_path)

    config_file = TestSettingFile(test_ini_path, "Test")
    print("config_file: {}".format(config_file))
    # try:
    #     while True:
    #         time.sleep(1.0)
    #         # print("running ...")

    # except (KeyboardInterrupt, SystemExit):
    #     exit()

# ===================== Main =====================
if __name__ == "__main__":
    test()