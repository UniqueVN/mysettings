from setuptools import Command, find_packages, setup

__version__ = "0.0.1"
setup(
    name = "my_settings",
    version = __version__,
    description = "A simple settings helper module",
    long_description = "A simple Python module to help manage application settings",
    url = "https://bitbucket.org/UniqueVN/mysettings",
    author = "Thang To",
    author_email = "uniquevn@gmail.com",
    license = "MIT",
    classifiers = [
        "Intended Audience :: Developers",
        "Topic :: Utilities",
        "License :: MIT",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.6",
    ],
    keywords = "settings",
    # packages=['my_settings'],
    # packages = find_packages('my_settings', 'test'),
    packages = find_packages(),
    # package_dir = {'': 'my_settings'},
    package_data = {
        'my_settings': ['my_settings/test/config/*']
        # TODO
    },
    install_requires = [
        # "cmd2"
    ],
    extras_require = {
        # "test": [ "pytest" ]
    },
    entry_points = {
        "console_scripts": [
            "test_settings=my_settings.test.test_settings:test",
        ]
    },
    # cmdclass = { 
    #     "test": 
    # }
    scripts=[],
)